# DABRegistry

This project is a React Single Page App that acts as a user interface to the dabdata microservice (https://gitlab.com/semwebsrv/cma/dabdata). The client app uses keycloak to obtain a JWT which can be used when authenticating with that service.

for UAT this project is deployed to http://radio-registry-uat.s3-website-us-east-1.amazonaws.com/

The project is continuously deployed by the gitlab pipeline at https://gitlab.com/semwebsrv/cma/dabregistry/-/pipelines
