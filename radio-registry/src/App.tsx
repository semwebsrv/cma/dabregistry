import * as React from 'react'
import { StoreContext, stores } from 'stores/stores'
import SecuredApp from './SecuredApp'

// eslint-disable-next-line no-unused-vars
import { AppProps } from './common/AppProps'

const App = ({ appconfig }: AppProps) => {

  console.log('Config: %o', appconfig)
  stores.holoAppStore.configureModules(appconfig)

  return (
      <StoreContext.Provider value={stores}>
        <SecuredApp />
      </StoreContext.Provider>
  )
}

// Export components we want client libraries to be able to import
export default App
