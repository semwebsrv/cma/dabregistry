import React from 'react'
import { DomainViewComponentProps } from 'common/DomainViewComponentProps'
import { observer } from 'mobx-react'
import { Card, CardTitle, CardBody, CardFooter } from '@patternfly/react-core';
import { Link } from "react-router-dom";

class MultiplexView extends React.Component<DomainViewComponentProps, any> {

  render() {
    console.log("MultiplexView %o", this.props.resource);

    const tr_rows = this.props.resource?.transmitters?.map((t) => (
      <tr key={t.transmitter.id}>
        <td><Link to={"/transmitters/"+t.transmitter.id}>{t.transmitter.name}</Link></td>
      </tr>
    ));

    const bs_rows = this.props.resource?.bearers?.map((b) => (
      <tr key={b.service.id}>
        <td><Link to={"/services/"+b.service.id}>{b.service.name}</Link></td>
      </tr>
    ));

    return (
      <Card>
        <CardTitle>Multiplex: {this.props.resource?.name}</CardTitle>
        <CardBody>
          <b>Id</b><br/>
          {this.props.resource?.id}<br/>
          <b>Record Owner</b><br/>
          {this.props.resource?.owner?.name}<br/>
          <b>Name</b><br/>
          {this.props.resource?.name}<br/>
          <b>Area</b><br/>
          {this.props.resource?.area}<br/>
          <b>Ensemble Id</b><br/>
          {this.props.resource?.ead}<br/>
          <b>Block</b><br/>
          {this.props.resource?.block}<br/>
          <b>Frequency</b><br/>
          {this.props.resource?.frequency}<br/>
          <b>License Number</b><br/>
          {this.props.resource?.licenseNumber}<br/>
          <b>Homepage</b><br/>
          {this.props.resource?.homepage}<br/>
          <b>Transmitters</b><br/>
          <table>
            <tbody>
              {tr_rows}
            </tbody>
          </table>
          <b>Broadcast Services</b><br/>
          <table>
            <tbody>
              {bs_rows}
            </tbody>
          </table>
        </CardBody>
        <CardFooter></CardFooter>
      </Card>
    )
  }
}

export default observer(MultiplexView)

