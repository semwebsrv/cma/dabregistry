import React from 'react'
import {
  Form,
  FormGroup,
  TextInput
} from '@patternfly/react-core';
import { DomainEditComponentProps } from 'common/DomainEditComponentProps'
import { observer } from 'mobx-react'
import SimpleManyToManyViaJoinTable from 'components/domain/SimpleManyToManyViaJoinTable'
import MPTransmitter from './MPTransmitter'
import MPStation from './MPStation'
import ManyToOne from 'components/domain/ManyToOne'

class MultiplexEdit extends React.Component<DomainEditComponentProps, any> {

  handleChange = (value, event ) => {
    if ( (event.target != null) && ( this.props.handleChange != null ) ) {
      this.props.handleChange(event.target.name, event.target.value);
    }
    else {
      console.log("event.target was null %o or %o",event.target,this.props.handleChange);
    }
  }

  updateManyToOne = (property, value) => {
    console.log("MultiplexEdit::updateManyToOne(%o,%o)",property,value);
    this.props.handleChange(property, value);
  }

  render() {
    console.log("MultiplexEdit -- context = %o",this.props.resource);

    const transmitter_column_heads = [
      { label: 'Transmitter Id' },
      { label: 'Transmitter Name' },
    ];
    const transmitter_column_data = [
      { accessPath: 'transmitter.id' },
      { accessPath: 'transmitter.name' }
    ];
    const bearer_column_heads = [
      { label: 'Service Id' },
      { label: 'Service Name' },
    ];
    const bearer_column_data = [
      { accessPath: 'service.id' },
      { accessPath: 'service.name', valueMarkup: {type:'resourceLink', resourceType:'service', idAccessPath:'service.id' }  }
    ];


    return (
      <Form isHorizontal>
        <FormGroup label="Name" isRequired fieldId="simple-form-name-01">
          <TextInput
            isRequired
            type="text"
            id="simple-form-name-01"
            name="name"
            value={this.props.resource.name}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup label="owner" isRequired fieldId="multiplex-owner-01">
          <ManyToOne relatedType="Party" resource={this.props.resource} property="owner" labelProperty="name" onChange={this.updateManyToOne} />
        </FormGroup>
        <FormGroup label="ead" isRequired fieldId="multiplex-ead-01">
          <TextInput isRequired type="text" id="multiplex-ead-01" name="ead" value={this.props.resource.ead} onChange={this.handleChange}/>
        </FormGroup>
        <FormGroup label="area" isRequired fieldId="multiplex-area-01">
          <TextInput isRequired type="text" id="multiplex-area-01" name="area" value={this.props.resource.area} onChange={this.handleChange}/>
        </FormGroup>
        <FormGroup label="block" isRequired fieldId="multiplex-block-01">
          <TextInput isRequired type="text" id="multiplex-block-01" name="block" value={this.props.resource.block} onChange={this.handleChange}/>
        </FormGroup>
        <FormGroup label="frequency" isRequired fieldId="multiplex-frequency-01">
          <TextInput isRequired type="text" id="multiplex-frequency-01" name="frequency" value={this.props.resource.frequency} onChange={this.handleChange}/>
        </FormGroup>
        <FormGroup label="licenseNumber" isRequired fieldId="multiplex-licenseNumber-01">
          <TextInput isRequired type="text" id="multiplex-licenseNumber-01" name="licenseNumber" value={this.props.resource.licenseNumber} onChange={this.handleChange}/>
        </FormGroup>
        <FormGroup label="homepage" fieldId="multiplex-homepage-01">
          <TextInput isRequired type="text" id="multiplex-homepage" name="homepage" value={this.props.resource.homepage} onChange={this.handleChange}/>
        </FormGroup>
        <FormGroup label="Transmitters" fieldId="multiplex-transmitters-01">
          <SimpleManyToManyViaJoinTable resource={this.props.resource}
                                        joinResource="Transmitter"
                                        property="transmitters"
                                        columnHeads={transmitter_column_heads}
                                        columnData={transmitter_column_data}
                                        editComponent={MPTransmitter}
                                        handleChange={this.updateManyToOne}
                                        resourceStore={this.props.resourceStore}
                                        />
        </FormGroup>
        <FormGroup label="Services" fieldId="multiplex-bearers-01">
          <SimpleManyToManyViaJoinTable resource={this.props.resource}
                                        joinResource="Bearer"
                                        property="bearers"
                                        columnHeads={bearer_column_heads}
                                        columnData={bearer_column_data}
                                        editComponent={MPStation}
                                        handleChange={this.updateManyToOne}
                                        resourceStore={this.props.resourceStore}
                                        />
        </FormGroup>
      </Form>
    )
  }
}

export default observer(MultiplexEdit)

