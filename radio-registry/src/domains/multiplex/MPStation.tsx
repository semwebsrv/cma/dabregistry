import React from 'react';
import { observer } from 'mobx-react'
import ManyToOne from 'components/domain/ManyToOne'
import { Form, FormGroup } from '@patternfly/react-core';

export interface MPStationProps {
    resource: any
    rootContext: any
    contextPath: string
    handleChange(property:string, value:object)
}

export const MPStation = (props:MPStationProps) => {

  console.log("MPStation %o %o",props, props.resource);

  const updateManyToOne = (property, value) => {
    // props.handleChange(property, value);
    console.log("WIP - this needs to have access to the store and the fully qualified path to the resource")
    console.log("update %s.%s=%o",props.contextPath,property,value);
    props.handleChange(props.contextPath+"."+property,value);
  }

  return (
    <Form isHorizontal>
      <FormGroup label="Multiplex" fieldId="">
        <span>{props.rootContext?.name}</span>
      </FormGroup>

      <FormGroup label="Broadcast Service" isRequired fieldId="">
        <ManyToOne relatedType="BroadcastService"
                   resource={props.resource}
                   property="service"
                   labelProperty="name"
                   onChange={updateManyToOne} />
        { JSON.stringify(props.resource, null, 2) }
      </FormGroup>
    </Form>
  )
}


export default observer(MPStation);
