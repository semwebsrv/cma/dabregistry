import React from 'react';
import { observer } from 'mobx-react'
import ManyToOne from 'components/domain/ManyToOne'
import { Form, FormGroup } from '@patternfly/react-core';

export interface MPTransmitterProps {
    resource: any
    rootContext: any
    contextPath: string
    handleChange(property:string, value:object)
}

export const MPTransmitter = (props:MPTransmitterProps) => {

  console.log("MPTransmitter %o %o",props, props.resource);

  const updateManyToOne = (property, value) => {
    props.handleChange(props.contextPath+"."+property,value);
  }

  return (
    <Form isHorizontal>
      <FormGroup label="Transmitter" fieldId="">
        <span>{props.rootContext?.name}</span>
      </FormGroup>

      <FormGroup label="Transmitter" isRequired fieldId="">
        <ManyToOne relatedType="Transmitter"
                   resource={props.resource}
                   property="transmitter"
                   labelProperty="name"
                   onChange={updateManyToOne} />
        { JSON.stringify(props.resource, null, 2) }
      </FormGroup>
    </Form>
  )
}


export default observer(MPTransmitter);
