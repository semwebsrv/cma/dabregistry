import React from 'react'
import { DomainViewComponentProps } from 'common/DomainViewComponentProps'
import { observer } from 'mobx-react'
import {Card, CardBody, CardTitle} from "@patternfly/react-core";

class PartyView extends React.Component<DomainViewComponentProps, any> {

  render() {
    return (
        <Card>
            <CardTitle>Party: {this.props.resource?.name}</CardTitle>
            <CardBody>
                <b>Id</b><br/>
                {this.props.resource?.id}<br/>
                <b>Shortcode</b><br/>
                {this.props.resource?.shortcode}<br/>
                <b>Name</b><br/>
                {this.props.resource?.name}<br/>
                <b>Owner</b><br/>
                {this.props.resource?.parent?.name}<br/>
                { process.env.NODE_ENV !== 'production' ? JSON.stringify(this.props.resource, null, 2) : '' }
            </CardBody>
        </Card>
    )
  }
}

export default observer(PartyView)

