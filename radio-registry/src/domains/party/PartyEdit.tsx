import React from 'react'
import {
  Form,
  FormGroup,
  TextInput
} from '@patternfly/react-core';
import { DomainEditComponentProps } from 'common/DomainEditComponentProps'
import { observer } from 'mobx-react'
import ManyToOne from 'components/domain/ManyToOne'

class PartyEdit extends React.Component<DomainEditComponentProps, any> {

  handleChange = (value, event ) => {
    console.log("PartyEdit::handleChange(%o,%o)",value,event);
    if ( (event.target != null) &&
         ( this.props.handleChange != null ) ) {
      this.props.handleChange(event.target.name, event.target.value);
    }
    else {
      console.log("target null or change handler null")
    }
  }

  updateManyToOne = (property, value) => {
      this.props.handleChange(property, value);
  }

  render() {
    return (
      <Form isHorizontal>

        <FormGroup label="Shortcode" isRequired fieldId="party-shortcode-01">
          <TextInput
              isRequired
              type="text"
              id="party-shortcode-01"
              name="shortcode"
              value={this.props.resource.shortcode}
              onChange={this.handleChange}
          />
        </FormGroup>

        <FormGroup label="Name" isRequired fieldId="party-name-01">
          <TextInput
            isRequired
            type="text"
            id="party-name-01"
            name="name"
            value={this.props.resource.name}
            onChange={this.handleChange}
          />
        </FormGroup>

        <FormGroup label="Owner" fieldId="">
          <ManyToOne relatedType="Party"
                     resource={this.props.resource}
                     property="parent"
                     labelProperty="name"
                     onChange={this.updateManyToOne}/>
        </FormGroup>

      </Form>
    )
  }
}

export default observer(PartyEdit)

