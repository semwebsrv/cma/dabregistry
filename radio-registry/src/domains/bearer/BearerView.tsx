import React from 'react'
import { DomainViewComponentProps } from 'common/DomainViewComponentProps'
import { observer } from 'mobx-react'
import {Card,
  CardBody,
  CardTitle,
  ExpandableSection,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription} from "@patternfly/react-core";

class BearerView extends React.Component<DomainViewComponentProps, any> {

  render() {
    return (
        <Card>
          <CardTitle>Bearer</CardTitle>
          <CardBody>
            <ExpandableSection toggleText="Main Details" isExpanded={true}>
              <DescriptionList columnModifier={{ lg: '2Col', xl: '3Col' }}>
                <DescriptionListGroup>
                  <DescriptionListTerm>ID</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource?.id}</DescriptionListDescription>
                </DescriptionListGroup>
                <DescriptionListGroup>
                  <DescriptionListTerm>Type</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource?.type}</DescriptionListDescription>
                </DescriptionListGroup>
                <DescriptionListGroup>
                  <DescriptionListTerm>From</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource?.startTimestampDisplay}</DescriptionListDescription>
                </DescriptionListGroup>
                <DescriptionListGroup>
                  <DescriptionListTerm>To</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource?.endTimestampDisplay}</DescriptionListDescription>
                </DescriptionListGroup>
              </DescriptionList>
            </ExpandableSection>
            { this.props.resource?.service != null &&
            <ExpandableSection toggleText="Service Details" isExpanded={true}>
              <DescriptionList columnModifier={{ lg: '2Col', xl: '3Col' }}>
                <DescriptionListGroup>
                  <DescriptionListTerm>Service Name</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource.service.name}</DescriptionListDescription>
                </DescriptionListGroup>
                <DescriptionListGroup>
                  <DescriptionListTerm>SID</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource.service.sid}</DescriptionListDescription>
                </DescriptionListGroup>
                <DescriptionListGroup>
                  <DescriptionListTerm>LSN</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource.service.lsn}</DescriptionListDescription>
                </DescriptionListGroup>
              </DescriptionList>
            </ExpandableSection>
            }
            { this.props.resource?.multiplex != null &&
            <ExpandableSection toggleText="Multiplex Details" isExpanded={true}>
              <DescriptionList columnModifier={{ lg: '2Col', xl: '3Col' }}>
                <DescriptionListGroup>
                  <DescriptionListTerm>Multiplex Name</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource.multiplex.name}</DescriptionListDescription>
                </DescriptionListGroup>
                <DescriptionListGroup>
                  <DescriptionListTerm>Ensemble Id</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource.multiplex.ead}</DescriptionListDescription>
                </DescriptionListGroup>
                <DescriptionListGroup>
                  <DescriptionListTerm>Area</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource.multiplex.area}</DescriptionListDescription>
                </DescriptionListGroup>
                <DescriptionListGroup>
                  <DescriptionListTerm>Block</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource.multiplex.block}</DescriptionListDescription>
                </DescriptionListGroup>
                <DescriptionListGroup>
                  <DescriptionListTerm>Frequency</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource.multiplex.frequency}</DescriptionListDescription>
                </DescriptionListGroup>
                <DescriptionListGroup>
                  <DescriptionListTerm>License #</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource.multiplex.licenseNumber}</DescriptionListDescription>
                </DescriptionListGroup>
                <DescriptionListGroup>
                  <DescriptionListTerm>Homepage</DescriptionListTerm>
                  <DescriptionListDescription>{this.props.resource.multiplex.homepage}</DescriptionListDescription>
                </DescriptionListGroup>
              </DescriptionList>
            </ExpandableSection>
            }
            <br/>
            { process.env.NODE_ENV !== 'production' ? JSON.stringify(this.props.resource, null, 2) : '' }
          </CardBody>
        </Card>    )
  }
}

export default observer(BearerView)

