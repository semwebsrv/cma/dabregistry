import React from 'react'
import {
  FormGroup,
  TextInput
} from '@patternfly/react-core';
import { DomainEditComponentProps } from 'common/DomainEditComponentProps'
import { observer } from 'mobx-react'

class BearerEdit extends React.Component<DomainEditComponentProps, any> {

  handleChange = (value, event ) => {
    if ( (event.target != null) && ( this.props.handleChange != null ) ) {
      this.props.handleChange(event.target.name, event.target.value);
    }
  }

  render() {
    return (
      <div>
        <FormGroup label="Id" isRequired fieldId="bearer-id-01">
          <TextInput
            isRequired
            type="text"
            id="bearer-id-01"
            name="id"
            value={this.props.resource.id}
            onChange={this.handleChange}
          />
        </FormGroup>
      </div>
    )
  }
}

export default observer(BearerEdit)

