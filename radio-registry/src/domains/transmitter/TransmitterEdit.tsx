import React from 'react'
import {
  FormGroup,
  TextInput
} from '@patternfly/react-core';
import { DomainEditComponentProps } from 'common/DomainEditComponentProps'
import { observer } from 'mobx-react'

class TransmitterEdit extends React.Component<DomainEditComponentProps, any> {

  handleChange = (value, event ) => {
    if ( (event.target != null) && ( this.props.handleChange != null ) ) {
      this.props.handleChange(event.target.name, event.target.value);
    }
  }

  render() {
    return (
      <div>
        <FormGroup label="Name" isRequired fieldId="transmitter-name-01">
          <TextInput
            isRequired
            type="text"
            id="transmitter-name-01"
            name="name"
            value={this.props.resource.name}
            onChange={this.handleChange}
          />
        </FormGroup>
      </div>
    )
  }
}

export default observer(TransmitterEdit)

