import React from 'react'
import { DomainViewComponentProps } from 'common/DomainViewComponentProps'
import { observer } from 'mobx-react'
import {Card, CardBody, CardTitle} from "@patternfly/react-core";

class TransmitterView extends React.Component<DomainViewComponentProps, any> {

  render() {
    return (
      <Card>
        <CardTitle>Transmitter: {this.props.resource?.name}</CardTitle>
        <CardBody>
          <b>Id</b><br/>
          {this.props.resource?.id}<br/>
          <b>Name</b><br/>
          {this.props.resource?.name}<br/>
        </CardBody>
      </Card>
    )
  }
}

export default observer(TransmitterView)

