import { makeObservable, action, observable, onBecomeObserved } from "mobx"


export default class SessionStore {

  user : object = {};

  constructor() {
    console.log("SessionStore::constructor");
    makeObservable(this, {
      user: observable,
      setUser: action
    })

    onBecomeObserved(this, "user", (() => console.log("Someone is watching the user")))
  }

  setUser(new_user_details: object) {
    console.log('setUser(%o)', new_user_details)
    // for(var k in new_user_details) this.user[k]=new_user_details[k];
    this.user = new_user_details
  }

}

