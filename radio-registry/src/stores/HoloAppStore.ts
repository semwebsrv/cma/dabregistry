// @ts-nocheck

import { makeObservable, action, observable } from "mobx"
import axios from 'axios'
import Keycloak, { KeycloakInstance, KeycloakConfig } from 'keycloak-js'

export default class HoloAppStore {

  // The default endpoint for requests
  axios: object

  // The String URL of the base endpoint
  baseURL: string

  // Config loaded from server
  loadedConfig: object

  authenticated: boolean = false

  keycloak: KeycloakInstance | null

  initialised: boolean = false

  moduleRegister: any | null = {}

  // The app can know about resource types - for example "DirectoryEntry" that can be referenced
  // in many different places. This register holds the info that allows the apps to create links,
  // render full and brief representations, create typedowns for selection, show fully fledged
  // search forms or edit resources of the given type. This is the core of the CRUD mechanism in
  // holo
  resourceTypeRegister:any | null = {}

  sidebarVisible: boolean = false

  constructor() {
    console.log("SessionStore::constructor");
    this.init({});
    makeObservable(this, {
      authenticated: observable,
      initialised: observable,
      sidebarVisible: observable,
      toggleSidebar: action,
      setInitialised: action,
      setAuthenticated: action
    })
  }


  toggleSidebar() {
    this.sidebarVisible = !this.sidebarVisible
    console.log("ToggleSidebar %o",this.sidebarVisible)
  }

  setInitialised(value: boolean) {
    console.log('Setting initialised to %o', value)
    this.initialised = value
  }

  setAuthenticated(value: boolean) {
    this.authenticated = value;
    // Get the
    console.log("Setting authenticated: %o",value);
    if ( value ) {
      // Inject the keycloak JWT into the axios client as a header
      // https://www.techynovice.com/setting-up-JWT-token-refresh-mechanism-with-axios/
      console.log("Adding Keycloak JWT into axios headers: %o",this.keycloak.token);
      this.axios.defaults.headers.common['Authorization'] = 'Bearer '+this.keycloak.token;
    }
  }

  async init(app_config) {
    console.log('initialise HoloServerApi app_config=%o', app_config)
    console.log("Fetch /holoreact/rrconf.json from same source as app");

    const owner: HoloAppStore = this

    return await window
      .fetch('/rrconf.json', { cache: 'no-cache' })
      .then((response) => response.json())
      .then((config_json) => {
        owner.loadedConfig = config_json
        console.log('Loaded Config: %o %o', owner.loadedConfig)

        owner.baseURL = config_json.serviceBaseUrl;
        if ( owner.baseURL == null ) {
          console.log("serviceBaseURL key not found in holoconf.json file");
        }
        else {
          console.log("Initialse holo base URL tp %s",owner.baseURL);
        }

        owner.axios = axios.create({
          baseURL: owner.baseURL,
          timeout: 10000,
          headers: {
            accept: 'application/json',
            'content-type': 'application/json'
          }
        })

      })
      .then(() => {
        console.log('Initialise keycloak.... %s/%s', this.loadedConfig.authrealm, this.loadedConfig.keycloakUrl)
        this.keycloak = Keycloak({
          realm: this.loadedConfig.authrealm,
          clientId: 'rrclient',
          url: this.loadedConfig.keycloakUrl // was url: 'http://localhost:3000/auth'
        } as KeycloakConfig)
      })
      .then(() => {
        this.setInitialised(true)
        return true
      })
      .catch(function (err) {
        console.log('Problem fetching rrconf.json', err)
        return false
      })
  }

  getAxios() {
    console.log('getAxios....')
    return this.axios
  }

  getTenant() {
    return this.tenant
  }

  // Query is a graphql query of the form "query($un: String!) { generalUserQuery(username: $un, max: 100) { totalCount results { id username } } }"
  graphQLoverJSON(graphql_path, p_query, p_variables) {
    const defaultParams = { stats: 'true', max: '20', offset: 0 }
    const mergedProps = { ...defaultParams, ...p_variables }

    console.log(
      'make graphql request to %s query is %s with variables %o,%o',
      graphql_path,
      p_query,
      p_variables,
      mergedProps
    )

    const graphql_request = {
      query: p_query,
      variables: mergedProps
    }

    const result = this.axios.post(this.baseURL + graphql_path, graphql_request)

    console.log(
      'GraphQL %o [tenant %s] response %o',
      graphql_request,
      this.tenant,
      result
    )

    return result
  }

  lookupType(resource_type) {
    console.log("Looking up resource type %o in register %o",resource_type, this.resourceTypeRegister);
    return this.resourceTypeRegister[resource_type]
  }

  configureModules(appconfig) {
    this.resourceTypeRegister = appconfig.resourceTypeRegister
  }

  restfulGet(resourceUriPath, params) {
    // return this.axios.get(resourceUriPath, mergedProps );
    return this.axios.get(resourceUriPath, { params: params })
  }

  restfulSearch(resourceUriPath, params) {
    const defaultParams = { stats: 'true', perPage: '10', setname:'full' }
    const mergedProps = { ...defaultParams, ...params }
    console.log('make search request with params %o', mergedProps)

    // return this.axios.get(resourceUriPath, mergedProps );
    return this.axios.get(resourceUriPath, { params: mergedProps })
  }

  restfulPost(resourceUriPath, resource) {
    return this.axios.post(resourceUriPath, resource);
  }

}
