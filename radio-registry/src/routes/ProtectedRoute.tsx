import React from 'react'
import { Route } from 'react-router-dom'
import { observer } from 'mobx-react'
import { StoreContext } from 'stores/stores'

class ProtectedRoute extends React.Component<any, any> {
  static contextType = StoreContext

  // componentDidMount() {
  //   console.log('Do authentication....')
  //   // Ask keycloak to authentication us
  //   this.context.holoAppStore.keycloak
  //     .init({ flow: 'standard', onLoad: 'login-required', enableLogging: true })
  //     .then(auth_info => {
  //       console.log('Authenticated:%o... load user profile', auth_info)
  //     })
  //     .catch(error => console.log("Error: %o",error))
  // }

  // THis stanza can be used to update the token if it has expired
  // keycloak.updateToken(30).success(function() {
  //     loadData();
  // }).error(function() {
  //     alert('Failed to refresh token');
  // );

  render() {
    // See https://scalac.io/user-authentication-keycloak-1/
    console.log('ProtectedRoute::render this.context = %o', this.context)
    return <Route {...this.props} />
  }
}
export default observer(ProtectedRoute)
