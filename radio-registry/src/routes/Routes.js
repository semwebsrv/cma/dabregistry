import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute'
import Dashboard from 'views/main/Dashboard';
import About from 'views/main/About';
import CRUDComponent from 'components/crud/CRUDComponent';
import ResourceComponent from 'components/resource/ResourceComponent';

const Routes = () => {

  return (
    <Switch>
      <Redirect exact from="/" to="/multiplexes" />
      <ProtectedRoute component={About} exact path="/about" />
      <ProtectedRoute component={Dashboard} exact path="/dashboard" />
      <ProtectedRoute exact path="/multiplexes" render={(props) => <CRUDComponent domain="Multiplex" {...props} />}/>
      <ProtectedRoute exact path="/multiplexes/:id" render={(props) => <ResourceComponent domain="Multiplex" resourceId={props.match.params.id} {...props} />}/>
      <ProtectedRoute exact path="/bearers" render={(props) => <CRUDComponent domain="Bearer" {...props} />}/>
      <ProtectedRoute exact path="/bearers/:id" render={(props) => <ResourceComponent domain="Bearer" resourceId={props.match.params.id} {...props} />}/>
      <ProtectedRoute exact path="/services" render={(props) => <CRUDComponent domain="BroadcastService" {...props} />}/>
      <ProtectedRoute exact path="/services/:id" render={(props) => <ResourceComponent domain="BroadcastService" resourceId={props.match.params.id} {...props} />}/>
      <ProtectedRoute exact path="/orgs" render={(props) => <CRUDComponent domain="Party" {...props} />}/>
      <ProtectedRoute exact path="/orgs/:id" render={(props) => <ResourceComponent domain="Party" resourceId={props.match.params.id} {...props} />}/>
      <ProtectedRoute exact path="/transmitters" render={(props) => <CRUDComponent domain="Transmitter" {...props} />}/>
      <ProtectedRoute exact path="/transmitters/:id" render={(props) => <ResourceComponent domain="Transmitter" resourceId={props.match.params.id} {...props} />}/>
      <Redirect to="/notfound" />
    </Switch>
  );
}

export default Routes;

