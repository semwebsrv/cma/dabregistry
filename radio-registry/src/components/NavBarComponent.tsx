import * as React from 'react'
// import { StoreContext, stores } from 'stores/stores'

import { Nav, NavItem, NavList } from  '@patternfly/react-core'

export const NavBarComponent = () => {

  // See https://github.com/BilalBouk/reactstrap-basic-sidebar
    // return (
    //   <NavItem key={module.id}>
    //     <NavLink to={module.rootpath}>
    //       {module.id}
    //     </NavLink>
    //   </NavItem>
    // )

  return (
    <Nav aria-label="Nav">
      <NavList>
        <NavItem key="Wibble">This is an item</NavItem>
      </NavList>
    </Nav>
  )
}

export default NavBarComponent
