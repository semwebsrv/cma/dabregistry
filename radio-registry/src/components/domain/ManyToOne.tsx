import React, { useState } from 'react';
import {Select, SelectOption, SelectOptionObject, SelectVariant} from '@patternfly/react-core';
import { useStore } from 'stores/stores'
import {observer} from "mobx-react";

export interface ManyToOneProps {
  relatedType: string
  resource: any                          // The actual parent object containing the data properties to be edited
  property: string                       // the property on resource that holds this list
  onChange?(property:string, value:any)  // The handler to call when we select a new option
  labelProperty: string                  // The property in relatedType that we will use as a label
}

interface ManyToOneResource extends SelectOptionObject{
    resource: any
    toString(): string
}
// Considering DropdownPanel instead for this....

export const ManyToOne = ({relatedType,
                           resource,
                           property,
                           onChange,
                           labelProperty
                           }:ManyToOneProps) => {

  // For access to server and utils
  const empty_options : {id:string, label:string, index:number, value:ManyToOneResource}[] = [];

  const store = useStore()
  const [isOpen, setIsOpen] = useState(false);
  const [options, setOptions] = useState( empty_options );
  var selected = null;

  const domain_config = store.holoAppStore.lookupType(relatedType)

  console.log("ManyToOne related domain config is %o",domain_config);

  const onToggle = () => {
    // console.log("ManyToOneProps::onToggle options==%o",options);
    setIsOpen(!isOpen);
    performSearch('%');
  };

  const onSelect = (event, selection, isPlaceholder) => {
    console.log("ManyToOneProps::onSelect(%o,%o,%o)",event,selection,isPlaceholder);
    if (onChange != null ) {
      console.log("Notify property change %o",property);
      onChange(property,selection['resource']);
    }
    else {
      console.warn("onChange is null");
    }
    setIsOpen(false);
  };

  const clearSelection = () => {
    console.log("ManyToOneProps::clearSelection");
  };

  const typeaheadChanged = (v) => {
      // console.log("typeaheadChanged: %s",v);
      performSearch(v);
  }

  const performSearch = (querystr) => {
    // console.log("performSearch %s",querystr);
    store.holoAppStore.restfulSearch(domain_config.resourcePath,
                                     {
                                       q: querystr,
                                       offset: 0,
                                       max: 20,
                                       setname: 'brief'
                                     }).then( (response) => {
      if ( response?.data != null ) {
          console.log("Got search results.... %o",response)
          let new_options : {id:string, label:string, index:number, value:any}[]= []
          let index = 0;
          response.data.resultList.forEach(e => {
              // console.log("Adding option %o", e)
              new_options.push({
                  id: e.id,
                  label: e[labelProperty],
                  index: index++,
                  value: {
                      resource: e,
                      toString: () => { return e[labelProperty] }
                  }
              })
          })
          setOptions(new_options);
      }
    })
  }

  let current_value = '';
  if ( ( resource != null ) && 
       ( resource[property] != null ) ) {
    current_value = resource[property]?.[labelProperty];
  }
  else{
      console.log("Unable to set current_value - resource(%o) is or prop(%s) is",resource,property)
  }

  return (
    <Select
          variant={SelectVariant.typeahead}
          typeAheadAriaLabel="Select a state"
          onToggle={onToggle}
          onSelect={onSelect}
          onClear={clearSelection}
          selections={current_value}
          onTypeaheadInputChanged={typeaheadChanged}
          isOpen={isOpen}
        >
      {options.map((option, index) => (
        <SelectOption
              key={option.id}
              value={option.value}
              index={option.index}
        />
      ))}
    </Select>
  )
}

export default observer(ManyToOne)
