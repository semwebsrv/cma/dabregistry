import React, { useState } from 'react';
import { Table, TableHeader, TableBody, TableProps, IRowData, IExtraData, IActionsResolver, TableVariant } from '@patternfly/react-table';
import { Modal, Button, ModalVariant } from '@patternfly/react-core';
import { PlusCircleIcon } from '@patternfly/react-icons';

import _ from 'lodash';

export interface SimpleManyToManyViaJoinTableProps {
  resource: any                            // The actual parent object containing the data properties to be edited
  joinResource: string                     // The kind of resource that this is a collection of - linking back to the app store
  property: string                         // the property on resource that holds this list
  columnHeads: any[]                       // The column headings we wish to show
  columnData: any[]                        // how to get and present the the column data values
  editComponent: React.ElementType         // The component used to create and edit new items in the collection
  handleChange(property:string, value:any) // Called to make any update
  resourceStore: object
}

export const SimpleManyToManyViaJoinTable = ({resource,
                                              joinResource,
                                              property,
                                              columnHeads,
                                              columnData,
                                              editComponent,
                                              handleChange}:SimpleManyToManyViaJoinTableProps) => {

  var column_headings : any[]= [];
  var table_data: any[] = [];
  const initial_selected_item : object = {};
  const [selectedCollectionItem, setSelectedCollectionItem] = useState(initial_selected_item);
  const [selectedItemIndex, setSelectedItemIndex] = useState(0);

  column_headings = columnHeads.map((coldef) => {
    return coldef.label;
  });

  console.log("Proprty is %s, Resource is %o",property, resource);
  console.log("List Prop is %o",resource[property]);

  if ( resource[property] == null ) {
    console.log("Initialising %s to empty list",property);
    handleChange(property,[]);
  }

  if ( resource[property] != null ) {
      table_data = resource[property].map((result_object) => {
        console.log("process row %o",result_object);
        var row_data: any[] = []
        columnData.map((coldef) => {
          row_data.push(_.get(result_object,coldef.accessPath));
        })
        return row_data;
      });
  }

  console.log("table_data: %o, head:%o",table_data,column_headings);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const handleModalToggle = () => {
    console.log("handleModalToggle");
    if ( isModalOpen ) {
      console.log("Closing open modal")
      // We are closing the modal - probably with the close button
    }
    else {
      // Create a new line in the collection and edit it
        const new_item_index = resource[property].length
        console.log("Create new collection item - index will be %d",new_item_index)
        setSelectedItemIndex(new_item_index);
        resource[property].push({})
        const new_collection_item:object = resource[property][new_item_index]
        setSelectedCollectionItem(new_collection_item);
    }
    setIsModalOpen( !isModalOpen );
  };

  // Need uppercase first letter for JSX
  const EditComponent = editComponent

  console.log("Render SimpleManyToManyViaJoinTable - selectedCollectionItem=%o",selectedCollectionItem)
  return (
    <div>
      <Table cells={column_headings}
             rows={ table_data }
             isStickyHeader
             aria-label="Aria Label"
             variant={TableVariant.compact} >
        <TableHeader />
        <TableBody />
      </Table>
      <Button onClick={handleModalToggle} style={{'float':'right'}} variant="link" icon={<PlusCircleIcon />} >Add</Button>
      <Modal
          title="Record List"
          isOpen={isModalOpen}
          variant={ModalVariant.large}
          showClose={true}
          onClose={handleModalToggle} >
        <EditComponent resource={selectedCollectionItem}
                       rootContext={resource}
                       contextPath={property+"["+selectedItemIndex+"" +"]"}
                       handleChange={handleChange}
        />
      </Modal>
    </div>
  )
}

export default SimpleManyToManyViaJoinTable
