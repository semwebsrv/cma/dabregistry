import React from 'react'
import { Nav, NavItem, NavList, PageSidebar } from '@patternfly/react-core'
// import classNames from 'classnames'
import { Link } from 'react-router-dom'

export const SideBar = ({ isNavOpen }) => {

  const HoloNav = (
     <Nav aria-label="Nav">
        <NavList>
          <NavItem id="dashboard" itemId={"dashboard"} isActive={false}><Link to="/dashboard">Dashboard</Link></NavItem>
          <NavItem id="multiplexes" itemId={"multiplexes"} isActive={false}><Link to="/multiplexes">Multiplexes</Link></NavItem>
          <NavItem id="bearers" itemId={"bearers"} isActive={false}><Link to="/bearers">Bearers</Link></NavItem>
          <NavItem id="services" itemId={"services"} isActive={false}><Link to="/services">Broadcast Services</Link></NavItem>
          <NavItem id="orgs" itemId={"orgs"} isActive={false}><Link to="/orgs">Organisations</Link></NavItem>
          <NavItem id="transmitters" itemId={"transmitters"} isActive={false}><Link to="/transmitters">Transmitters</Link></NavItem>
          <NavItem id="about" itemId={"about"} isActive={false}><Link to="/about">About</Link></NavItem>
        </NavList>
      </Nav>
  )

  return (
    // https://www.patternfly.org/v4/components/page#vertical-nav
    <PageSidebar nav={HoloNav} isNavOpen={isNavOpen}/>
  )
}

export default SideBar
