import * as React from 'react'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'
import CRUDStore from './CRUDStore'
import CRUDView from './CRUDView';
import {useLocation} from "react-router-dom";

const CRUDComponent = (props) => {

  const store = useStore()
  var query_params = new URLSearchParams(useLocation().search);
  var domainConfig = store.holoAppStore.lookupType(props.domain)

  if ( domainConfig == null ) {
    console.log("unable to locate domain config for %o",props.domain);
  }

  // const [ crudData ] = useState( new CRUDStore(domainConfig, store.holoAppStore) );
  console.log("Create a new crudData for all the children of this component and pass it down the tree")
  const crudData = new CRUDStore(domainConfig, store.holoAppStore);
  var q = query_params.get("q");
  if ( q ) {
    crudData.setQuery( { "luceneQueryString" : q } );
  }
  else{
    crudData.setQuery( { "luceneQueryString" : "%" } );
  }
  crudData.executeQuery();

  console.log("CrudData: %o", crudData);
  console.log("Domain config: %o",domainConfig);

  return (
    <CRUDView crudData={crudData}/>
  )
}

export default observer(CRUDComponent)
