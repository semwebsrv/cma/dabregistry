import { makeObservable, action, observable } from "mobx"
import HoloAppStore from "../../stores/HoloAppStore"

export default class CRUDStore {

  query : object = {
    luceneQueryString:''
  };

  resultsPage : Array<object> = [];
  domainConfig: null | object = null;
  holoAppStore: null | HoloAppStore = null;
  resultCount: null | number;
  selectedResource: null | object = null;
  selectedRow: null | number = null;
  selectedResourceId: null | String = null;
  showDetailsPanel: boolean = true;
  pageNumber: number = 1;
  pageSize : | number = 10;

  /**
   * The intersection of a server config, a config description of a resource domain (Effectively, a domain class in the back end)
   * and a user search input / result set
   */
  constructor(domainConfig: object, holoAppStore: HoloAppStore) {
    console.log("CRUDStore::constructor");
    this.domainConfig = domainConfig;
    this.holoAppStore = holoAppStore;
    this.resultCount = 0;
    this.pageNumber = 0;
    this.pageSize = 10;

    makeObservable(this, {
      query: observable,
      resultsPage: observable,
      setQuery: action,
      setResultsPage: action,
      executeQuery: action,
      resultCount: observable,
      selectedResource: observable,
      selectedRow: observable,
      selectedResourceId: observable,
      setSelectedResource: action,
      showDetailsPanel: observable,
      pageNumber: observable,
      setPageNumber: action,
      pageSize: observable
    })
  }

  setQuery(new_query: object) {
    console.log('setQuery(%o)', new_query)
    this.query = new_query
  }

  setResultsPage(new_results_page: Array<object>) {
    console.log('setResultsPage(%o)', new_results_page)
    this.resultsPage = new_results_page
  }

  setShowDetailsPanel(v: boolean) {
    if ( this.showDetailsPanel != v ) {
      console.log("update show details panel to %o",v);
      this.showDetailsPanel = v;
    }
  }

  setPageNumber(n: number) {
    if ( ( this.holoAppStore != null ) &&
         ( this.domainConfig != null ) ) {
      var lucene_query = this.query['luceneQueryString']
      this.holoAppStore.restfulSearch(this.domainConfig['resourcePath'],
                                      {
                                        q: lucene_query,
                                        offset: ((n-1)*this.pageSize),
                                        max: this.pageSize,
                                        setname: 'brief'
                                      }).then ((response) => {
        console.log("Handle response %o",response);
        if ( response?.data != null ) {
          var query_results = response.data;
          this.resultCount = query_results.totalCount;
          this.resultsPage = query_results.resultList;
          this.pageNumber = n;
          if ( ( query_results.resultList != null ) &&
               ( query_results.resultList.length > 0 ) &&
               ( this.showDetailsPanel === true ) ) {
            // this.setSelectedResource(query_results.resultList[0]);
            this.setSelectedRow(0);
          }
          else {
            console.log("Result list %o length %d %o",query_results.resultList,
                                                   query_results.resultList?.length, this.showDetailsPanel);
          }
        }
      })
    }
  }

  executeQuery() {
    console.log("CRUDStore::executeQuery query:%o domainConfig:%o",this.query, this.domainConfig);
    if ( ( this.holoAppStore != null ) &&
         ( this.domainConfig != null ) ) {
      this.setShowDetailsPanel(true)
      this.setPageNumber(1);
    }
  }

  setSelectedResource(resource: object) {
    console.log("setSelectedResource(%o)",resource);
    this.selectedResource=resource;
  }

  // User is selecting a row in the result set - we get the clicked row number
  setSelectedRow(rownum: number){

    this.selectedRow = rownum
    if ( this.domainConfig != null && this.holoAppStore != null ) {
      this.selectedResourceId = this.domainConfig['getResourceId'](this.resultsPage[rownum]);
    }

    /*
    // If we have a valid domain config
    if ( this.domainConfig != null &&
         this.holoAppStore != null ) {
      // Call the domain config getResourceId function to extract a resource id from the json object
      // that is the row in the table
      const resource_id = this.domainConfig['getResourceId'](this.resultsPage[rownum]);

      this.holoAppStore.restfulGet(this.domainConfig['resourcePath']+'/'+resource_id,
          {
            setname: 'full'
          }).then ((response) => {
        console.log("Handle response %o", response);
        this.setSelectedResource(response.data);
      })

      // Now retrieve the full rendition of that object (We assume the object used in the result set is a
      // BRIEF rendering of the full record).
      console.log("setSelectedRow(%d) resource id is %s",rownum, resource_id);
    }
    */
  }
}

