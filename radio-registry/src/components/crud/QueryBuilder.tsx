import React, { useEffect } from 'react'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'
import CRUDStore from './CRUDStore'
import { AppProps } from 'common/AppProps'
import { PlusCircleIcon } from '@patternfly/react-icons';
import { Link } from "react-router-dom";
import {
  Form,
  InputGroup,
  TextInput,
  Button
} from '@patternfly/react-core';

interface QueryBuilderProps {
  crudStore: CRUDStore;
}

const QueryBuilder = ({crudStore}:QueryBuilderProps) => {

  const store = useStore()
  console.log('Stores: holoStore:%o CRUDstore:%o', store, crudStore)

  // var query_params = new URLSearchParams(useLocation().search);

  /*
  useEffect(() => {
    var q = query_params.get("q");
    if ( q ) {
      console.log("Use query %s",q);
      handleTextChange(q);
    }
    else {
      console.log("wildcard search");
      handleTextChange('%');
    }
    handleQuery();
  }, [])
   */

  const handleTextChange = value => {
    console.log("handleTextChange %o %o",value,crudStore);
    crudStore.setQuery( { "luceneQueryString" : value } );
  }

  const handleQuery = (event) => {
    console.log("handleQuery");
    crudStore.executeQuery();
    event.preventDefault();
  }

  const noop = (event) => {
    console.log("noop");
    event.preventDefault();
  }

  var current_query_string = crudStore.query['luceneQueryString']
  if ( current_query_string == null)
    current_query_string = ''

  console.log("Query string: %o",current_query_string);

  var new_resource_path='';
  if ( crudStore.domainConfig != null ) {
    new_resource_path = crudStore.domainConfig['appPath']+'/__new__';
  }

  return (
    <Form onSubmit={handleQuery}>
      <InputGroup>
        <TextInput id="qry"
                   isRequired type="text"
                   name="simple-form-name-01"
                   aria-describedby="simple-form-name-01-helper"
                   onChange={handleTextChange}
                   value={current_query_string} />
        <Button type="submit" variant="primary">Search</Button>
        <Button variant="link" icon={<PlusCircleIcon />} ><Link to={new_resource_path}>New</Link></Button>
      </InputGroup>
    </Form>
  )
}

export default observer(QueryBuilder)

