import * as React from 'react'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'
import QueryBuilder from './QueryBuilder'
import QueryResults from './QueryResults'
import DetailPanel from './DetailPanel'
import { Grid, GridItem } from '@patternfly/react-core';
import { Stack, StackItem } from '@patternfly/react-core';
import CRUDStore from './CRUDStore'

const CRUDView = (props) => {

  const store = useStore()
  var crudData = props.crudData;

  // Split should be replaced with https://www.patternfly.org/v4/components/drawer#drawer - (Resizable on inline)
  let search_span : 1|2|3|4|5|6|7|8|9|10|11|12 = 6;

  if ( crudData.showDetailsPanel ) {
    console.log("show details panel");
  }
  else {
    console.log("don't show details panel");
  }

  return (
    <Grid hasGutter>
      <GridItem span={search_span}>
        <Stack>
          <StackItem>
            <QueryBuilder crudStore={crudData}/>
          </StackItem>
          <StackItem isFilled>
            <QueryResults crudStore={crudData} />
          </StackItem>
        </Stack>
      </GridItem>
      <GridItem span={6}>
        <DetailPanel crudStore={crudData} />
      </GridItem>
    </Grid>
  )
}

export default observer(CRUDView)
