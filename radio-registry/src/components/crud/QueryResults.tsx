import * as React from 'react'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'
import {
  Table,
  TableHeader,
  TableBody, TableProps, IRowData, IExtraData, IActionsResolver, TableVariant
} from '@patternfly/react-table';
import CRUDStore from './CRUDStore'
import { Pagination } from '@patternfly/react-core';

import _ from 'lodash';

interface QueryResultsProps {
  crudStore: CRUDStore
}

// This implementation is TABLE based query results - at some point we may have alternate
// presenations such as card based results
const QueryResults = ({crudStore}:QueryResultsProps) => {

  const store = useStore()
  console.log('Stores: %o %o', store, crudStore)

  var column_headings : any[]= [];
  var table_data: any[] = [];
  var domain_plural_label = "";
  var query_string = "";
  const result_count = crudStore.resultCount || 0;

  if ( crudStore.domainConfig != null ) {
    column_headings = crudStore.domainConfig['tabularResults']['columns']?.map((coldef) => {
      return coldef.label;
    });
    domain_plural_label = crudStore.domainConfig['plural']
  }
  else {
    console.error("domain config is missing tabularResults.columns definition of the results table");
  }

  if ( crudStore.query != null ) {
    query_string = crudStore.query['luceneQueryString']
  }

  if ( ( crudStore.resultsPage != null ) &&
       ( crudStore.domainConfig != null ) ) {

    var domain_config = crudStore.domainConfig;

    table_data = crudStore.resultsPage.map((result_object) => {
      console.log("Process data row %o",result_object)
      var row_data: any[] = []
      domain_config['tabularResults']['columns']?.map((coldef) => {
        // result.push(result_object);
        console.log("  - process column %o", coldef)
        let col_value = _.get(result_object,coldef.accessPath,'')
        console.log("Value will be %o", col_value)
        row_data.push(col_value);
      })

      return row_data;
    })
  }

  const action_resolver : IActionsResolver = function(rowData:IRowData, extraData:IExtraData) : any {
    console.log("actionResolver...");
    return [
      // {
      //   title: 'Edit',
      //   onClick: (event, rowId, rowData, extra) => {console.log("Wibble")}
      // }
    ];
  }

  const row_click_handler = function(event, object, row_props,computed_data) {
    console.log("row click %o, %o, %o, %o",event,object, row_props, computed_data);
    // Selected row is row_props.rowIndex
    crudStore.setSelectedRow(row_props.rowIndex);

    // This will be removed shortly
    // crudStore.setSelectedResource(crudStore.resultsPage[row_props.rowIndex]);


    if ( crudStore.showDetailsPanel === false ) {
      console.log("showDetails is false, set it to true");
      crudStore.setShowDetailsPanel(true);
    }
  }

  const onSetPage = (_event, pageNumber)  => {
    console.log("onSetPage %o",pageNumber);
    crudStore.setPageNumber(pageNumber);
  }

  const onPerPageSelect = (_event, perPage) => {
    console.log("onPerPageSelect");
  }


  console.log("Column_headings: %o, table_data",column_headings,table_data);

  // Used to list Query {query_string} in {domain_plural_label} found {result_count} records

  return (
    <div>
       <Pagination
        itemCount={result_count}
        perPage={10}
        page={crudStore.pageNumber}
        onSetPage={onSetPage}
        widgetId="pagination-options-menu-top"
        onPerPageSelect={onPerPageSelect}
      />
      <div style={{overflowX: "scroll", overflowY: "visible"}}>
        <Table cells={column_headings}
               rows={ table_data }
               actionResolver={ action_resolver }
               aria-label="{result_count} {domain_plural_label} found for query {query_string}"
               isStickyHeader
               variant={TableVariant.compact}
               >
          <TableHeader />
          <TableBody onRowClick={row_click_handler} />
        </Table>
      </div>
    </div>
  )
}

export default observer(QueryResults)

