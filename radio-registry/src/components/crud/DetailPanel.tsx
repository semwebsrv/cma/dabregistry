import React, { useEffect } from 'react'
import { observer } from 'mobx-react'
import CRUDStore from './CRUDStore'
import ResourceComponent from 'components/resource/ResourceComponent'

interface DetailPanelProps {
  crudStore: CRUDStore;
}

const DetailPanel = ({crudStore}:DetailPanelProps) => {

  var ViewComponent: null | React.ElementType = null;

  if (crudStore.domainConfig != null) {
    // const resource_id = crudStore.domainConfig['getResourceId'](crudStore.resultsPage[rownum]);
    return (
      <ResourceComponent domain={crudStore.domainConfig['name']} resourceId={crudStore.selectedResourceId} />
    )
  }
  else {
    return <p>No View</p>
  }
}

export default observer(DetailPanel)
