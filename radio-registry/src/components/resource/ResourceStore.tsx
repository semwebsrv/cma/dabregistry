import {makeObservable, observable, action, toJS} from "mobx"
import HoloAppStore from "../../stores/HoloAppStore"
import lodash from 'lodash'

export default class ResourceStore {

  resourceId: null | String = null;
  resource: null | object = null;
  holoAppStore: HoloAppStore;
  domainConfig: object;
  mode: String = 'view'

  /**
   * The intersection of a server config, a config description of a resource domain (Effectively, a domain class in the back end)
   * and a user search input / result set
   */
  constructor(domainConfig: object, holoAppStore: HoloAppStore) {
    // console.log("ResourceStore::constructor");
    this.holoAppStore = holoAppStore;
    this.domainConfig = domainConfig;

    makeObservable(this, {
      resource: observable,
      mode: observable,
      updateResource: action,
      setResourceId: action,
      setResource: action,
      setMode: action
    })
  }

  setResourceId(resource_id: String) {
    // console.log("ResourceStore::setResourceId(%s)",resource_id);
    if ( resource_id === "__new__" ) {
      const new_record_template = this.domainConfig['newRecordTemplate'];
      if ( new_record_template ) {
        // console.log("Initialise new record template %o",new_record_template())
        this.resource = new_record_template();
        this.setMode('edit');
      }
      else{
        // console.log("empty object initialisation");
        this.resource = {};
      }
    }
    else {
      // This is the stub where we retrieve the resource
      // console.log("fetch resource");
      this.resourceId = resource_id;
      this.fetchResource(resource_id);
    }
  }

  setResource(resource: any) {
    this.resource = resource
  }

  fetchResource(resource_id: String) {

    console.log("fetch resource %s",resource_id);

    if ( this.domainConfig != null &&
         this.holoAppStore != null &&
         resource_id != null ) {
      // Call the domain config getResourceId function to extract a resource id from the json object
      // that is the row in the table
      this.holoAppStore.restfulGet(this.domainConfig['resourcePath']+'/'+resource_id, { setname: 'full' })
        .then ((response) => {
          // console.log("FR--Handle response %o", response);
          this.setResource(response.data);
          // this.setSelectedResource(response.data);
        })
    }
  }

  updateResource(jsonpath: string, value:any) {
    // console.log("store.updateResource(%s,%s)",jsonpath,value);
    lodash.set(this.resource, jsonpath, value);
    console.log("ResourceStore::updateResource(%s,%o) result: %o",jsonpath,value,toJS(this.resource));
  }

  saveResource() {
    console.log("Save Resource %o",toJS(this.resource));
    var resource_as_json = toJS(this.resource)
    // this.holoAppStore.restfulSearch(this.domainConfig['resourcePath'], { q: lucene_query }).then ((response) => {
    return this.holoAppStore.restfulPost(this.domainConfig['resourcePath'], resource_as_json)
        .then((response) => {
          console.log("restfulPost response %o",response);
          return response
        });
  }

  setMode(mode: String) {
    // console.log("setMode %s",mode);
    this.mode = mode;
  }

  resourceAsJson() {
    return toJS(this.resource);
  }

}

