import * as React from 'react'
import { useStore } from 'stores/stores'
import { useState } from "react"
import { observer } from 'mobx-react'
import ResourceStore from './ResourceStore'
import {
  ActionGroup,
  Form,
  Button
} from '@patternfly/react-core';
import { useHistory } from "react-router-dom";

const EditResource = (props) => {

  const store = useStore()
  const history = useHistory()
  console.log('Stores: %o, Props: %o - resource ID will be %o', store, props, props.match.params.id)

  var domainConfig = store.holoAppStore.lookupType(props.domain)
  var EditComponent: null | React.ElementType = null;
  var find_self_query: null | string;

  // https://gitlab.com/semwebsrv/holo/monospa/holoreact/-/blob/spike_mobx/src/components/crud/RecordViewer.js
  const [ resourceStore ] = useState( new ResourceStore(domainConfig, store.holoAppStore) );

  const handleChange = (propname, value) => {
    console.log("handleChange prop=%o value=%o",propname,value);
    if ( ( resourceStore != null ) &&
         ( resourceStore.resource != null ) ) {
      resourceStore.updateResource(propname,value)
    }
  }

  const saveClose = () => {
    var resource_as_json = resourceStore.saveResource().then ((response) => {
      console.log("Save completed.. forward to search page %s")
      findSelf();
    });
  }

  const findSelf = () => {
    var find_self_query = domainConfig.findSelfQuery(resourceStore.resourceAsJson());
    var new_path = domainConfig.appPath+"?q="+find_self_query;
    console.log("Find self query will be %s, new path will be %s",find_self_query,new_path);
    history.push(new_path);
  }

  const saveNew = () => {
    resourceStore.saveResource().then ((response) => {
      console.log("Save complete");
    });
  }

  if ( ( domainConfig == null ) &&
       ( domainConfig.editView != null ) ) {
    console.log("unable to locate domain config for %o",props.domain);
  }
  else {
    console.log("Domain config: %o",domainConfig);

    switch ( domainConfig.editView['type'] ) {
      case 'Component':
        EditComponent = domainConfig.editView['component']
        break;
      default:
        console.log("Unhandled domain config %o",domainConfig);
        break;
    }
  }

  resourceStore.setResourceId(props.match.params.id);

  if ( EditComponent != null ) {
    return (
      <div>
        <p>Edit Resource Container</p>
        <Form>
          <EditComponent handleChange={handleChange} resource={resourceStore.resource} />
          <ActionGroup>
            <Button variant="primary" onClick={saveClose}>Save+Close</Button>
            <Button variant="primary" onClick={saveNew}>Save+New</Button>
          </ActionGroup>
        </Form>
      </div>
    )
  }
  else {
    return ( <p>Error</p> )
  }
}

export default observer(EditResource)

