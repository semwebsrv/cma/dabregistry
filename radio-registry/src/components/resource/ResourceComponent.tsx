import * as React from 'react'
import { useStore } from 'stores/stores'
import { useState } from "react"
import { observer } from 'mobx-react'
import ResourceStore from './ResourceStore'
import {
  ActionGroup,
  Form,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardHeaderMain,
  CardActions
} from '@patternfly/react-core';
import { useHistory } from "react-router-dom";


/**
 * ResourceComponent.
 * Render a resource - a resource can be in 2 modes view or edit. View is a read only display optimised layout,
 * edit is for the creation and maintenance tasks. This top level component handles both and allows the user to
 * switch between the different views
 * the property recordid is used to convey the record id and the property domain is used to convey the type information
 */
const ResourceComponent = (props) => {

  const store = useStore()
  const history = useHistory()
  console.log('ResourceComponent::Stores: %o, Props: %o - resource ID will be %o', store, props, props.resourceId)

  let domainConfig = store.holoAppStore.lookupType(props.domain)
  let InternalResourceComponent: null | React.ElementType = null;
  const [ resourceStore ] = useState( new ResourceStore(domainConfig, store.holoAppStore) );

  if ( resourceStore.domainConfig != domainConfig)
    resourceStore.domainConfig = domainConfig;

  const toggleEdit = ( () => {
    console.log("toggleEdit");
    resourceStore.setMode('edit');
  });

  const saveAndRefresh = ( () => {
    console.log("saveAndRefresh");
    // resourceStore.setMode('view');
    resourceStore.saveResource();
  });

  const abandonChanges = ( () => {
    console.log("saveAndRefresh");
    resourceStore.setMode('view');
  });

  console.log("ResourceComponent set resourceId to %s",props.resourceId);
  if ( ( resourceStore.resourceId == null ) ||
       ( resourceStore.resourceId != props.resourceId ) ) {
    // Change of resource we are looking at, reload
    resourceStore.setResourceId(props.resourceId);
  }

  let actions_array: JSX.Element[] = [];

  if ( domainConfig == null ) {
    console.log("unable to locate domain config for %o",props.domain);
  }
  else {
    console.log("Domain config: %o",domainConfig);
    if ( resourceStore.mode === 'view' ) {
      switch ( domainConfig.view['type'] ) {
        case 'Component':
          InternalResourceComponent = domainConfig.view['component']
          break;
        default:
          console.log("Unhandled domain config for resource view %o",domainConfig);
          break;
      }
      actions_array.push(<Button key="btn_edit" variant="tertiary" onClick={toggleEdit}>Edit</Button>)
    }
    else {
      switch ( domainConfig.editView['type'] ) {
        case 'Component':
          InternalResourceComponent = domainConfig.editView['component']
          break;
        default:
          console.log("Unhandled domain config for resource edit %o",domainConfig);
          break;
      }
      if ( props.resourceId != '__new__' )
        actions_array.push(<Button key="btn_abandon" variant="tertiary" onClick={abandonChanges}>Abandon</Button>)
      actions_array.push(<Button key="btn_save" variant="tertiary" onClick={saveAndRefresh}>Save</Button>)
    }
  }

  if ( InternalResourceComponent != null ) {
    return (
      <Card>
        <CardHeader>
          <CardHeaderMain>
            {props.domain} : {props.resourceId}
          </CardHeaderMain>
          <CardActions>
            {actions_array}
          </CardActions>
        </CardHeader>
        <CardBody>
          <InternalResourceComponent resourceStore={resourceStore}
                                     resource={resourceStore.resource}
                                     handleChange={resourceStore.updateResource}/>
        </CardBody>
      </Card>
    )
  }
  else {
    return ( <p>Error</p> )
  }
}

export default observer(ResourceComponent)

