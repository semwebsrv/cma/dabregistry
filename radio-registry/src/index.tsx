import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import MultiplexEdit from './domains/multiplex/MultiplexEdit'
import MultiplexView from "./domains/multiplex/MultiplexView";
import TransmitterEdit from './domains/transmitter/TransmitterEdit'
import TransmitterView from './domains/transmitter/TransmitterView'
import PartyEdit from './domains/party/PartyEdit'
import PartyView from './domains/party/PartyView'
import BroadcastServiceEdit from './domains/broadcastService/BroadcastServiceEdit'
import BroadcastServiceView from './domains/broadcastService/BroadcastServiceView'
import BearerEdit from './domains/bearer/BearerEdit'
import BearerView from './domains/bearer/BearerView'

const defaultResourceIdFunction = function(resource:object) {
  return resource['id'];
};

const appcfg:any ={
  resourceTypeRegister:{
    'Multiplex':{
      name:'Multiplex',
      plural:'Multiplexes',
      resourcePath:'/dabdata/multiplex',
      getResourceId: defaultResourceIdFunction,
      appPath:'/multiplexes',
      tabularResults:{
        columns:[
          { label:'ID', accessPath:'id' },
          { label:'Name', accessPath:'name' },
          { label:'Ensemble ID', accessPath:'ead' },
          { label:'License#', accessPath:'licenseNumber' }
        ]
      },
      editView: {
        type:'Component',
        component: MultiplexEdit
      },
      view:{
        type:'Component',
        component: MultiplexView
      },
      findSelfQuery: function (resource: object) {
        return "name: \""+resource['name']+"\"";
      },
      newRecordTemplate: function() {
        return {
          id:'',
          name:'',
          ead:'',
          area:'',
          block:'',
          frequency:'',
          licenseNumber:'',
          homepage:''
        }
      }
    },
    'Party': {
      name: 'Party',
      plural: 'Parties',
      resourcePath: '/dabdata/party',
      getResourceId: defaultResourceIdFunction,
      appPath: '/orgs',
      tabularResults: {
        columns: [
          {label: 'ID', accessPath: 'id'},
          {label: 'Name', accessPath: 'name'},
          {label: 'Owner', accessPath: 'parent.name'}
        ]
      },
      editView: {
        type: 'Component',
        component: PartyEdit
      },
      view:{
        type:'Component',
        component: PartyView
      },
      findSelfQuery: function (resource: object) {
        return "name: \"" + resource['name'] + "\"";
      },
      newRecordTemplate: function () {
        return {
            id: null,
            name: '',
            shortcode: '',
            parent: null
        }
      },
    },
    'Transmitter':{
      name:'Transmitter',
      plural:'Transmitters',
      resourcePath:'/dabdata/transmitter',
      getResourceId: defaultResourceIdFunction,
      appPath:'/transmitters',
      tabularResults:{
        columns:[
          { label:'ID', accessPath:'id' },
          { label:'Name', accessPath:'name' },
        ]
      },
      editView: {
        type:'Component',
        component: TransmitterEdit
      },
      view:{
        type:'Component',
        component: TransmitterView
      },
      findSelfQuery: function (resource: object) {
        return "name: \""+resource['name']+"\"";
      },
      newRecordTemplate: function () {
        return {
          id: null,
          name: null
        }
      }
    },
    'BroadcastService':{
      name:'BroadcastService',
      plural:'Broadcast Services',
      resourcePath:'/dabdata/broadcastService',
      getResourceId: defaultResourceIdFunction,
      appPath:'/services',
      tabularResults:{
        columns:[
          { label:'ID', accessPath:'id' },
          { label:'Name', accessPath:'name' },
        ]
      },
      editView: {
        type:'Component',
        component: BroadcastServiceEdit
      },
      view:{
        type:'Component',
        component: BroadcastServiceView
      },
      findSelfQuery: function (resource: object) {
        return "name: \""+resource['name']+"\"";
      },
      newRecordTemplate: function () {
        return {
          id: null,
          name: null
        }
      }
    },
    'Bearer':{
      name:'Bearer',
      plural:'Bearers',
      resourcePath:'/dabdata/bearer',
      getResourceId: defaultResourceIdFunction,
      appPath:'/bearers',
      tabularResults:{
        columns:[
          { label:'ID', accessPath:'id' },
          { label:'Type', accessPath:'type' },
          { label:'Broadcast Service', accessPath:'service.name' },
          { label:'Multiplex', accessPath:'multiplex.name' },
          { label:'From', accessPath:'startTimestampDisplay' },
          { label:'To', accessPath:'endTimestampDisplay' },
        ]
      },
      editView: {
        type:'Component',
        component: BearerEdit
      },
      view:{
        type:'Component',
        component: BearerView
      },
      findSelfQuery: function (resource: object) {
        return "name: \""+resource['name']+"\"";
      },
      newRecordTemplate: function () {
        return {
          id: null,
          name: null
        }
      }
    }
  }
}

ReactDOM.render(
  <React.StrictMode>
    <App appconfig={appcfg} />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

