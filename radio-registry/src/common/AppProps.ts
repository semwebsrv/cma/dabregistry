import { History, Location} from 'history'
import { match } from 'react-router-dom'

export interface AppProps {
  classes?: any
  history?: History
  location?: Location
  match?: match
  appconfig: any
}
