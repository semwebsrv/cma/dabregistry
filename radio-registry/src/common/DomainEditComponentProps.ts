import { History, Location} from 'history'
import { match } from 'react-router-dom'

export interface DomainEditComponentProps {
  resource: any
  resourceStore: object
  handleChange(name, value)
}
