import React from 'react'
import { observer } from 'mobx-react'
import { StoreContext } from 'stores/stores'
import { AppProps } from '../../common/AppProps'
import { Gallery, GalleryItem } from '@patternfly/react-core';
import { Card, CardTitle, CardBody, CardFooter } from '@patternfly/react-core';
import DataloadWidget from './DataloadWidget'
import { Button } from '@patternfly/react-core';


class Dashboard extends React.Component<AppProps, any> {

  static contextType = StoreContext

  requestDataImport(url:string) {
    console.log("Request data import %s",url);
  };

  // eslint-disable-next-line no-useless-constructor
  // constructor(props: AppProps) {
  //   super(props)
  // }

  render() {
    console.log("render dash");

    return (
      <div>
          <Gallery hasGutter>
              <GalleryItem>
                  <Card>
                      <CardTitle>Status</CardTitle>
                      <CardBody> The system currently contains
                           <ul>
                               <li>nnn Transmitters</li>
                               <li>nnn Ensembles/Multiplexes</li>
                               <li>nnn Broadcast services</li>
                               <li>nnn Bearer Records</li>
                           </ul>
                      </CardBody>
                  </Card>
              </GalleryItem>

              <GalleryItem>
                  <DataloadWidget/>
              </GalleryItem>
              <GalleryItem>
                  <Card>
                      <CardTitle>.csv Export</CardTitle>
                      <CardBody>
                          <Button>Export</Button>
                      </CardBody>
                  </Card>
              </GalleryItem>

          </Gallery>
      </div>
    )
  }
}

export default observer(Dashboard)
