import React from 'react'
import { observer } from 'mobx-react'
import { StoreContext } from 'stores/stores'
import { AppProps } from '../../common/AppProps'

class About extends React.Component<AppProps, any> {

  static contextType = StoreContext


  // eslint-disable-next-line no-useless-constructor
  // constructor(props: AppProps) {
  //   super(props)
  // }

  render() {

    return (
      <div>
        <p>
          About
        </p>
      </div>
    )
  }
}

export default observer(About)
