import * as React from 'react'
import { useStore } from 'stores/stores'
import { Card, CardTitle, CardBody, CardFooter } from '@patternfly/react-core';
import {
    Form,
    InputGroup,
    TextInput,
    Button
} from '@patternfly/react-core';


const DataloadWidget = () => {

  const store = useStore()
  console.log("got store %o", store);
  let upload_details = {
    source_url:'https://www.ofcom.org.uk/__data/assets/file/0018/91305/TxParamsDAB.csv',
    status:'IDLE'
  }

  const requestDataImport = ( () => {
    console.log("Request data import %s", upload_details.source_url);
    if ( ( upload_details.source_url != null ) &&
         ( upload_details.source_url.length > 0 ) ) {
      store.holoAppStore.restfulGet('/dabdata/loadDataFromUrl', {source:upload_details.source_url} ).then ((response) => {
        console.log("Handle response %o",response);
        if ( response?.data != null ) {
          console.log("Response data %o",response.data);
        }
      })
    }

  });

  const updateSourceURL = ( ( value:string ) => {
    upload_details.source_url = value;

  })

  return (
    <Card>
      <CardTitle>OFCOM Sync</CardTitle>
      <CardBody>Load CSV Data from URL:
        <Form>
          <TextInput onChange={updateSourceURL} name="sourceUrl" value={upload_details?.source_url} />
          <Button onClick={requestDataImport}>Sync</Button>
        </Form>
      </CardBody>
    </Card>
  )
}

export default DataloadWidget

