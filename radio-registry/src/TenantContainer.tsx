import React, { useEffect } from "react";
import { createBrowserHistory } from 'history'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'
import { Router } from 'react-router-dom'
import Routes from 'routes/Routes'
import { SideBar } from 'components/SideBar'
import UserMenu from 'components/UserMenu'
import { Page,
         PageSection,
         PageHeader,
         PageHeaderTools,
         PageHeaderToolsItem } from '@patternfly/react-core';


const browserHistory = createBrowserHistory()

// See https://www.patternfly.org/v4/components/page/react-demos
// Also  https://github.com/priley86/patternfly-react-starter/blob/master/docs/routing-and-navigation.md

/**
 * The purpose of tenantContainer is to configure the app for a given context. Usually
 * the tenant is resolved by looking up the domain name.
 */
const TenantContainer = () => {

  const store = useStore()
  console.log('Stores: %o', store)

  // Adding this does seem to make a difference
  // console.log('User: %o', store.sessionStore.user)

  useEffect(() => {
    if ( ( store.sessionStore != null ) &&
         ( store.holoAppStore.initialised === true ) &&
         ( store.holoAppStore.keycloak != null ) ) {

      console.log("Calling keycloak init");

      store.holoAppStore.keycloak
        .init({ flow: 'standard', onLoad: 'login-required', enableLogging: true })
        .then(auth_info => {
          console.log('Authenticated:  %o',auth_info);

          // This will cause the store to capture the keycloak.token and add it to the list of headers
          // that the default axios client will send along with any requests
          store.holoAppStore.setAuthenticated(true);

          return store?.holoAppStore?.keycloak?.loadUserInfo()?.success(function(user_info) {
            console.log("Loaded user info %o",user_info);
            store?.sessionStore?.setUser(user_info);
          })
        })
        .catch(error => console.log("Error: %o",error))
    }
    else {
      console.log("KC NOT INITIALISED");
    }
  }, [ store]);


  let toggler = ( () => {
    store.holoAppStore.toggleSidebar();
  });

  const ConfiguredSideBar = ( <SideBar isNavOpen={store.holoAppStore.sidebarVisible} /> );

  // Inspired by https://github.com/patternfly/patternfly-react-demo-app/blob/d1c9a0c896d52a6d1564e066f2e9f7e2da8bd0ba/src/App.js
  const HeaderTools = (
    <PageHeaderTools>
      <PageHeaderToolsItem>
        <UserMenu />
      </PageHeaderToolsItem>
    </PageHeaderTools>
  );

  const Header = (
    <PageHeader showNavToggle
                logo="RadioRegistry"
                headerTools={HeaderTools}
                isNavOpen={store.holoAppStore.sidebarVisible}
                onNavToggle={toggler} />
  );

  if ( ( store.holoAppStore.initialised === true) &&
       ( store.holoAppStore.authenticated ) ) {

    if ( store?.holoAppStore?.keycloak?.isTokenExpired(10) ) {
      console.log("Token will expire in < 10s");
    }

    return (
      <Router history={browserHistory}>
        <Page mainContainerId="holoActiveComponent"
              header={Header}
              sidebar={ConfiguredSideBar}
              isManagedSidebar>
          <PageSection isFilled={true} hasOverflowScroll={true}>
            <Routes />
          </PageSection>
        </Page>
      </Router>
    )
  } else {
    return <p>Waiting for auth</p>
  }
}

// wondering if this should really be useObserver on the return from the function from 6.x or mobx-react-lite
export default observer(TenantContainer)
