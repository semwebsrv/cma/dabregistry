

# 2021-02-26 - 0.1.0

- Implement pagination controls
- Executing a search will show the details of the first listed result record in the right hand panel
- Add list of transmitters and services to multiplex display
- Search results now use "brief" setname, and "full" record is fetched only to display details panel. This helps prevent lag when searching, particularly with larger page sizes.
